# Generated by Django 3.0.5 on 2020-04-30 11:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuari',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('about', models.TextField(blank=True, max_length=400)),
                ('token', models.CharField(blank=True, max_length=100)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Submit',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200)),
                ('url', models.CharField(blank=True, max_length=100)),
                ('text', models.CharField(blank=True, max_length=400)),
                ('token', models.CharField(max_length=100)),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name='date published')),
                ('likes', models.IntegerField(default=0)),
                ('path', models.CharField(max_length=100)),
                ('qttcom', models.IntegerField(default=0)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
