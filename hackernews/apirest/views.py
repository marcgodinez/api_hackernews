from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from apirest.models import Submit,Usuari,Comment, Like_Submit,Like_Comment, Like_Submit
from apirest.serializers import SubmitSerializer, UsuariSerializer, UserSerializer, CommentSerializer,SubmitCommentSerializer, LikeCommentSerializer, LikeSubmitSerializer
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS, AllowAny
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
import sys
from rest_framework.views import APIView
from rest_framework import permissions
from django.shortcuts import get_object_or_404
from collections import namedtuple
Timeline = namedtuple('Timeline', ('submit', 'comentarios'))


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS

class IsPostRequest(permissions.BasePermission):

       def has_permission(self, request, view):
           return request.method == "POST"
class IsPutRequest(permissions.BasePermission):

       def has_permission(self, request, view):
           return request.method == "PUT"

###SUBMITS
##Ver todas las submits que hay
class Viewsubmits(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request):
        if request.method=='GET':
            submits = Submit.objects.filter(text__exact='')
            serializer = SubmitSerializer(submits, many=True)
            return JSONResponse(serializer.data)

##Añadir una submit
class Viewaddsubmit(APIView):
    def post(self,request):
        if request.method=='POST':
            data = JSONParser().parse(request)
            data['author'] = request.user.id
            if ('url' not in data and 'text' in data) or ('text' not in data and 'url' in data):
                if 'url' in data:
                    web=data['url']
                    if web.split('.')[0]=="http://www" or web.split('.')[0]=="https://www" or web.split('.')[0]=="www":
                        data['path']=web.split('.')[1] + "." + web.split('.')[2]
                    else: return JSONResponse("La URL introducida no es correcta")
                serializer = SubmitSerializer(data=data)
                if serializer.is_valid():
                    serializer.save()
                    return JSONResponse(serializer.data, status=201)
                return JSONResponse(serializer.errors, status=400)
            else:
                return JSONResponse("Los campos url y text no pueden estar los dos vacíos ni los dos llenos a la vez.",status=400)



##Ver una submit en concreto (pasando el id)
class Viewsubmitind(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request, pk):
        try:
            submit = Submit.objects.get(pk=pk)
        except Submit.DoesNotExist:
            return HttpResponse(status=404)
        if request.method=='GET':
            comentarios = Comment.objects.filter(submit=pk)
            timeline = Timeline(
                submit=Submit.objects.get(pk=pk),
                comentarios = Comment.objects.filter(submit=pk),
                )
            serializer = SubmitCommentSerializer(timeline)
            return JSONResponse(serializer.data)
    def post(self,request,pk):
        if request.method=='POST':
            submit = Submit.objects.get(pk=pk)
            data = JSONParser().parse(request)
            data['author'] = request.user.id
            data['submit']=submit.id
            submit.qttcom = submit.qttcom + 1
            submit.save()
            serializer = CommentSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return JSONResponse(serializer.data, status=201)
            return JSONResponse(serializer.errors, status=400)

##Ver todas las submits que hay por fecha
class Viewnewlist(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request):
        if request.method == 'GET':
            submits = Submit.objects.all().order_by('-date_added')
            serializer = SubmitSerializer(submits, many=True)
            return JSONResponse(serializer.data)

##Ver todas las submits que hay que son ask
class Viewasklist(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request):
        if request.method == 'GET':
            submits = Submit.objects.filter(url__exact='')
            serializer = SubmitSerializer(submits, many=True)
            return JSONResponse(serializer.data)

###USUARIOS
##Crear usuario nuevo
@csrf_exempt
def post(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            usuari = User.objects.get(username__exact=serializer.data['username'])
            usuario = Usuari.objects.get(user=usuari.id)
            serializer2=UsuariSerializer(usuario)
            return JSONResponse(serializer2.data, status=201)
        return JSONResponse(serializer.errors, status=400)

##Ver usuario individual
class ViewUserind(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request,pk):
        if request.method == 'GET':
            usuario= User.objects.get(pk=pk)
            usuari = Usuari.objects.get(user=usuario.id)
            if not usuari.user.id==request.user.id:
                usuari.token=''
            serializer = UsuariSerializer(usuari)
            return JSONResponse(serializer.data)
    def put(self,request,pk):
        if request.method == 'PUT':
            usuario= User.objects.get(pk=pk)
            usuari = Usuari.objects.get(user=usuario.id)
            data = JSONParser().parse(request)
            if usuari.user.id==request.user.id:
                usuari.about=data['about']
                usuari.save()
                serializer = UsuariSerializer(usuari)
                return JSONResponse(serializer.data, status=201)
            else: return JSONResponse("error", status=400)

class ViewUserindUpdate(APIView):
    permission_classes = [AllowAny]
    def put(self,request,pk):
        if request.method == 'PUT':
            usuario= User.objects.get(pk=pk)
            usuari = Usuari.objects.get(user=usuario.id)
            data = JSONParser().parse(request)
            if usuari.user.id==request.user.id:
                usuari.about=data['about']
                usuari.save()
                serializer = UsuariSerializer(usuari)
                return JSONResponse(serializer.data, status=201)
            else: return JSONResponse("error", status=400)

##Ver submits mias
class ViewUserSubmits(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request):
        if request.method == 'GET':
            submits = Submit.objects.filter(author__exact = request.user.id)
            serializer = SubmitSerializer(submits,many=True)
            return JSONResponse(serializer.data)

class ViewUserindSubmits(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request,pk):
        if request.method == 'GET':
            user = User.objects.get(pk=pk)
            submits = Submit.objects.filter(author__exact = user.id)
            serializer = SubmitSerializer(submits,many=True)
            return JSONResponse(serializer.data)

###COMENTARIOS
class ViewUserindComments(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request,pk):
        if request.method == 'GET':
            user = User.objects.get(pk=pk)
            comments = Comment.objects.filter(author__exact = user.id)
            serializer = CommentSerializer(comments,many=True)
            return JSONResponse(serializer.data)

class Viewcommentind(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request,pk):
        if request.method=='GET':
            submits = Comment.objects.get(pk=pk)
            serializer = CommentSerializer(submits)
            return JSONResponse(serializer.data)
    def post(self,request,pk):
        if request.method=='POST':
            comment = Comment.objects.get(pk=pk)
            data = JSONParser().parse(request)
            level = comment.level + 1
            data['level'] = level
            data['reply_to']=comment.author.id
            data['parent']=comment.id
            data['author'] = request.user.id
            data['submit']=comment.submit.id
            submit = Submit.objects.get(pk=comment.submit.id)
            submit.qttcom = submit.qttcom + 1
            submit.save()
            serializer = CommentSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return JSONResponse(serializer.data, status=201)
            return JSONResponse(serializer.errors, status=400)

class ViewUserindCommentsLike(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request,pk):
        if request.method == 'GET':
            user = User.objects.get(pk=pk)
            comments = Like_Comment.objects.filter(user__exact = user.id)
            serializer = LikeCommentSerializer(comments,many=True)
            return JSONResponse(serializer.data)

class ViewUserindSubmitsLike(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def get(self,request,pk):
        if request.method == 'GET':
            user = User.objects.get(pk=pk)
            submits = Like_Submit.objects.filter(user__exact = user.id)
            serializer = LikeSubmitSerializer(submits,many=True)
            return JSONResponse(serializer.data)

###LIKES


class Viewaddlikesubmitid(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def put(self,request,pk):
        if request.method=='PUT':
            submit = Submit.objects.get(pk=pk)
            post = get_object_or_404(Submit, id = pk)
            try:
                obj = Like_Submit.objects.get(user = request.user, post = post)
                serializer = SubmitSerializer(submit)
                return JSONResponse("Ya has dado like a esta publicacion", status=401)
            except Like_Submit.DoesNotExist:
                LikeSubmitId = Like_Submit()
                LikeSubmitId.user = request.user
                LikeSubmitId.post = post
                LikeSubmitId.save()
                submit.likes += 1
                submit.save()
                serializer = SubmitSerializer(submit)
                return JSONResponse(serializer.data, status=201)
            return JSONResponse(serializer.data, status=401)
        return JSONResponse(serializer.errors, status=400)


class Viewadddislikesubmitid(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def put(self,request,pk):
        if request.method=='PUT':
            submit = Submit.objects.get(pk=pk)
            post = get_object_or_404(Submit, id = pk)
            try:
                obj = Like_Submit.objects.get(user = request.user, post = post)
                submit.likes -= 1
                submit.save()
                obj.delete()
                serializer = SubmitSerializer(submit)
                return JSONResponse(serializer.data, status=201)
            except Like_Submit.DoesNotExist:
                serializer = SubmitSerializer(submit)
                return JSONResponse("No le has dado like a esta publicacion", status=401)
            return JSONResponse(serializer.data, status=401)
        return JSONResponse(serializer.errors, status=400)


class Viewaddlikecommentid(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def put(self,request,pk):
        if request.method=='PUT':
            comment = Comment.objects.get(pk=pk)
            comm = get_object_or_404(Comment, id = pk)
            try:
                obj = Like_Comment.objects.get(user = request.user, comment = comm)
                serializer = CommentSerializer(comment)
                return JSONResponse("Ya has dado like a este comentario",status=401)
            except Like_Comment.DoesNotExist:
                LikeCommentId = Like_Comment()
                LikeCommentId.user = request.user
                LikeCommentId.comment = comm
                LikeCommentId.save()
                comment.likes += 1
                comment.save()
                serializer = CommentSerializer(comment)
                return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)

class Viewadddislikecommentid(APIView):
    permission_classes = [IsAuthenticated|ReadOnly]
    def put(self,request,pk):
        if request.method=='PUT':
            comment = Comment.objects.get(pk=pk)
            comm = get_object_or_404(Comment, id = pk)
            try:
                obj = Like_Comment.objects.get(user = request.user, comment = comm)
                comment.likes -= 1
                comment.save()
                obj.delete()
                serializer = CommentSerializer(comment)
                return JSONResponse(serializer.data, status=201)
            except Like_Comment.DoesNotExist:
                return JSONResponse("No le has dado like a este comentario", status=401)
        return JSONResponse(serializer.errors, status=400)
