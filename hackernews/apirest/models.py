# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.urls import reverse #Used to generate URLs by reversing the URL patterns
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
import sys
from mptt.models import MPTTModel, TreeForeignKey

# Create your models here.
class Submit(models.Model):
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=100,blank=True)
    text = models.CharField(max_length=400,blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date_added = models.DateTimeField('date published', auto_now_add=True)
    likes = models.IntegerField(default=0)
    path = models.CharField(max_length=100,blank=True)
    qttcom = models.IntegerField(default=0)


    def __str__(self):

        return self.title

class Usuari(models.Model):
        #id = models.IntegerField(primary_key=True)
        user = models.ForeignKey(User, on_delete=models.CASCADE)
        about = models.TextField(max_length=400,blank=True)
        token = models.CharField(blank=True, max_length=100)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Usuari.objects.create(user=instance)


class Comment(MPTTModel):
    submit = models.ForeignKey('Submit', on_delete=models.CASCADE, null=True)
    text = models.TextField(max_length=400)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date_added = models.DateTimeField('date published', auto_now_add=True)
    likes = models.IntegerField(default=0)
    nivel = models.IntegerField(default=0)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children'
    )

    # Added, record secondary comments to whom, str
    reply_to = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='replyers'
    )

    # Replace Meta with MPTTMeta
    # class Meta:
    #     ordering = ('created',)
    class MPTTMeta:
        order_insertion_by = ['date_added']

class Like_Submit(models.Model):
    post = models.ForeignKey('Submit', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(auto_now= True)


class Like_Comment(models.Model):
    comment = models.ForeignKey('Comment', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(auto_now= True)

# This code is triggered whenever a new user has been created and saved to the database
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
       if created:
           token = Token.objects.create(user=instance)
           usuari = Usuari.objects.get(user=instance)
           usuari.token = token.key
           usuari.save()
