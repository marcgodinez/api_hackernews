from django.conf.urls import url
from apirest import views


urlpatterns = [
    url(r'^$', views.Viewsubmits.as_view()),
    url(r'^submit/$', views.Viewaddsubmit.as_view()),
    url(r'^item/(?P<pk>\d+)/$', views.Viewsubmitind.as_view()),
    url(r'^comment/(?P<pk>\d+)/$', views.Viewcommentind.as_view()),
    url(r'^new/$', views.Viewnewlist.as_view()),
    url(r'^ask/$', views.Viewasklist.as_view()),
    url(r'^login/$', views.post),
    url(r'^user/(?P<pk>\d+)/$', views.ViewUserind.as_view()),
    url(r'^user/(?P<pk>\d+)/submits/$', views.ViewUserindSubmits.as_view()),
    url(r'^user/(?P<pk>\d+)/comments/$', views.ViewUserindComments.as_view()),
    url(r'^user/(?P<pk>\d+)/comments/like/$', views.ViewUserindCommentsLike.as_view()),
    url(r'^user/(?P<pk>\d+)/submits/like/$', views.ViewUserindSubmitsLike.as_view()),
    #url(r'^user/(?P<pk>\d+)/update/$', views.ViewUserindUpdate.as_view()),
    url(r'^threads/$', views.ViewUserSubmits.as_view()),
    url(r'^item/(?P<pk>\d+)/like/$', views.Viewaddlikesubmitid.as_view()),
    url(r'^item/(?P<pk>\d+)/dislike/$', views.Viewadddislikesubmitid.as_view(),name="delete"),
    url(r'^comment/(?P<pk>\d+)/like/$', views.Viewaddlikecommentid.as_view()),
    url(r'^comment/(?P<pk>\d+)/dislike/$', views.Viewadddislikecommentid.as_view(),name="delete"),

    ]
