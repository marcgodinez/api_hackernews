from rest_framework import serializers
from apirest.models import Submit, Usuari,Comment,Like_Comment, Like_Submit
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
import sys
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password','groups','user_permissions','is_staff','is_active','is_superuser','last_login','date_joined')

class UsuariSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuari
        fields = ('user', 'about', 'token')
class SubmitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submit
        fields = ('id','author','title', 'url', 'text', 'date_added','likes', 'path', 'qttcom')
class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id','submit','text','author','date_added','likes','level','parent','reply_to')

class SubmitCommentSerializer(serializers.Serializer):
    submit = SubmitSerializer()
    comentarios= CommentSerializer(many=True)

class LikeCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Like_Comment
        fields = ('comment','user')

class LikeSubmitSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Like_Submit
        fields = ('post','user')
