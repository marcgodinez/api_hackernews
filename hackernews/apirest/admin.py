# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import User
from .models import Submit, Usuari, Comment,Like_Submit,Like_Comment

# Register your models here.
admin.site.register(Submit)
admin.site.register(Usuari)
admin.site.register(Comment)
admin.site.register(Like_Submit)
admin.site.register(Like_Comment)
