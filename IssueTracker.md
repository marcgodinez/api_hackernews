# DISTRIBUCIÓ DE FEINA #

## Edu/Marc/Adri ##

### Submit / ask ###

* Crear submit url
* Crear submit ask
* View submit new
* View submit ask
 
### Comentaris ###

* Crear comments
* Crear Replies
* Veure els comments de l’usuari

### Usuari ###

* Crear user
* Veure els comments als que l’usuari ha donat like
* Veure els submits als que l’usuari ha donat like

## Documentacio ##

* Swagger


## Oriol/Sergi ##

### Likes/Dislikes ###

* Crear like a la submits
* Treure like a la submit
* Crear like al comment
* Treure el like al comment

### Documentacio ###

* Swagger


## Problemes ##

Per aquesta segona part ens hem dividit la feina en 2 grups i d’aquesta manera facilitar la programació en parelles i la comunicació d’aquestes. Un primer grup format per Marc, Edu i Adri s’ha centrat en l’usuari, les submits/asks i els comentaris, mentres que l’altre grup format per l’Oriol i Sergi es centrava en els likes/dislikes del les publicacions i dels comentaris. 
Aquest cop hem pogut reutilitzar una gran part del codi de la part anterior el que ens ha agilitzat la feina junt amb l’ajuda de Postman.

